'use strict';

module.exports = app => {

    var userController = require('./user-controller');
    var deckController = require('./deck-controller');
    var upload = require('multer')({ dest: './uploads' });

    app.all("/api/*", function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
        return next();
    });

    app.route('/api/utilisateurs')
        .get(userController.get_all_users)
        .post(upload.single('avatar'), userController.create_a_user);

    app.route('/api/utilisateur/:id')
        .get(userController.find_a_user_by_id)
        .delete(userController.delete_a_user); 

    app.route('/api/utilisateur/auth')
        .post(userController.find_a_user_by_login_password);

    app.route('/api/utilisateur/:user/deck')
        .get(deckController.get_deck);

    app.route('/api/pokemon/add')
        .post(deckController.add_a_pokemon_to_deck);

    app.route('/api/utilisateur/deck/remove')
        .post(deckController.remove_a_pokemon_from_deck);

    app.route('/api/utilisateur/money')
        .post(userController.set_user_money);
    
};