var mongoose = require('mongoose');
var md5 = require('md5');
var fs =  require('fs');

User = mongoose.model('Users');

exports.create_a_user = function(req, res) {
    var new_user = new User();
    var avatarPath = "";
    new_user.avatar = "";
    new_user.login = req.body.login;
    new_user.password = md5(req.body.password);
    new_user.name = req.body.name;
    new_user.surname = req.body.surname;
    new_user.pokepoints = req.body.pokepoints;

    if(req.body.avatar.filename != '')
    {
        avatarPath = generateFilename(req.body.avatar.filename.split('.')[1]);
        fs.writeFile('uploads/' + avatarPath, req.body.avatar.value, {encoding: 'base64'}, function(err) {
            console.log('File created');
            new_user.avatar = avatarPath;
            
            new_user.save(function(err, user) {
                if(err) res.send(err);
                res.json(user);
            });
        });
    }
    else
    {
        new_user.save(function(err, user) {
            if(err) res.send(err);
            res.json(user);
        });
    }
};

exports.get_all_users = function(req, res) {
    User.find({}, function(err, users) {
        if (err) res.send(err);
        res.json(users);
    });
};

exports.delete_a_user = function (req, res) {
    User.remove({ _id: req.params.id }, function (err, user) {
        if (err) res.send(err);
        res.json({ message: 'User successfully deleted' });
    });
}; 

exports.find_a_user_by_id = function(req, res) {
    User.findById(req.params.userId, function(err, user) {
        if (err) res.send(err);
        res.json(user);
    });
};

exports.find_a_user_by_login_password = function(req, res) {
    User.find({login: req.body.login, password: md5(req.body.password)}, function(err, user) {
        if (err) res.send(err);
        if(user.length > 0 && user[0].avatar != "") user[0].avatar = global.config.appHost + '/uploads/' + user[0].avatar;
        res.json(user);
    });
};

exports.set_user_money = function(req, res) {
    User.findOneAndUpdate({ login: req.body.login }, { pokepoints: req.body.pokepoints }, function(err, user) {
        if (err) res.send(err);
        res.json(user);
    });
};

function generateFilename(extension = 'jpg') // Attention la chaine générée n'est pas unique
{
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.split('');
    var filename = '';

    for(var i = 0; i < 20; i++)
    {
        filename += chars[Math.round(Math.random()*(chars.length-1))];
    }

    filename += '.' + extension;

    console.log(filename);

    return filename;
}