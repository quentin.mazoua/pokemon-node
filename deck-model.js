'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DeckSchema = new Schema ({
    user: {
        type: String,
        unique: true,
        required: 'user is required'
    },
    pokemons: {
        type: Array,
        default: []
    },
    added_date: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Decks', DeckSchema);