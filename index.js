'use strict';

var http = require('http');
var url = require('url');
var express = require('express');
var mongoose = require('mongoose');
var routes = require('./routes.js');
var Users = require('./user-model.js');
var Decks = require('./deck-model.js');
var bodyParser = require('body-parser');
const config = require('./config.json');
global.config = config;

var app = express();
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));
app.use('/uploads', express.static('uploads'));

var port = process.env.PORT || 8080;

routes(app);

mongoose.Promise = global.Promise;
mongoose.connect(global.config.mongoURL);

app.listen(port);