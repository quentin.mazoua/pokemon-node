var mongoose = require('mongoose');

Deck = mongoose.model('Decks');

exports.add_a_pokemon_to_deck = function(req, res) {
    Deck.findOne({ user: req.body.user }, function(err, deck) {
        if(err) res.send(err);

        if(deck) // Si le deck existe
        {
            deck.pokemons.push(req.body.pokemon);
            deck.save(function(err2, deckSaved) {
                if(err2) res.send(err2);
                res.json(deckSaved);
            });
        }
        else // Si le deck n'existe pas
        {
            var new_deck = new Deck();

            new_deck.user = req.body.user;
            new_deck.pokemons.push(req.body.pokemon);

            new_deck.save(function(errCreateDeck, newDeckCreated) {
                if(errCreateDeck) res.send(errCreateDeck);
                res.json(newDeckCreated);
            });
        }
    });
};

exports.get_deck = function(req, res) {
    Deck.findOne({ user: req.params.user }, function(err, deck) {
        if (err) res.send(err);
        res.json(deck);
    });
};

exports.remove_a_pokemon_from_deck = function(req, res) {
    Deck.findOne({ user: req.body.user }, function(err, deck) {
        if (err) res.send(err);

        if(deck)
        {
            var pokemonIndex = deck.pokemons.findIndex(element => { return element == req.body.pokemon });

            if(pokemonIndex != -1)
            {
                deck.pokemons.splice(pokemonIndex, 1);

                deck.save(function(errSave, deckSaved) {
                    if(errSave) res.send(errSave);
                    res.json(deckSaved);
                });
            }
        }
        else
        {
            res.json(deck);
        }
    });
};

