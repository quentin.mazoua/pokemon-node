'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema ({
    login: {
        type: String,
        required: 'Login of the user is required',
        unique: true
    },
    password: {
        type: String,
        required: 'Password of the user is required'
    },
    surname: {
        type: String,
    },
    name: {
        type: String,
    },
    avatar: {
        type: String
    },
    pokepoints: {
        type: Number,
        default: 100
    },
    added_date: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Users', UserSchema);